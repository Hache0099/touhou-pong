extends KinematicBody2D

enum states {IDLE, MOVE, ATTACK, DASH}

var curve = load("res://new_curve.tres")

const MAX_SPEED : float = 350.0
const DASH_VEL : float = 150.0
const STOP_FORCE : float = 90.0
const ACCEL : float = 150.0

var move : Vector2
var dash_move : Vector2

var attacking : bool = false
var canShoot : bool = true

var currentState : int

func _ready():
	pass

func _physics_process(delta):
	checkState()
	$Label.set_text(str(currentState) + "\n" + str(move))

func checkState():
	match currentState:
		states.IDLE:
#			if abs(move.x) > 10.0:
#				currentState = "MOVE"
			$hurtbox.position.x = 0.0
			_check_movement()
			_check_attack()
			_shoot()
		states.MOVE:
#			if abs(move.x) < 300.0:
#				state = "IDLE"
			_apply_movement()
			_check_dash()
			_shoot()
		states.ATTACK:
			move_and_slide(Vector2.ZERO)
			if !attacking:
				_attack()
		states.DASH:
#			move.x = clamp(move.x,-MAX_SPEED - DASH_VEL, MAX_SPEED + DASH_VEL)
			if !attacking:
				_apply_dash()
			move_and_slide(dash_move)

#func _check_punch(is_idle : bool):
#	if Input.is_action_just_pressed("punch"):
#		if is_idle:
#			currentState = states.ATTACK
#		else:
#			currentState = states.DASH

func _check_attack():
	if Input.is_action_just_pressed("punch"):
		currentState = states.ATTACK

func _check_dash():
	if Input.is_action_just_pressed("punch"):
		currentState = states.DASH

func _check_movement():
	if Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
		currentState = states.MOVE
		return
	move.x = move_toward(move.x,0,STOP_FORCE)

func _attack():
	attacking = true
	$AnimationPlayer.play("attack")
	$hurtbox.position.x = 0.0

func _apply_dash():
	attacking = true
	var temp = move.x
	dash_move.x = (MAX_SPEED + DASH_VEL) * sign(temp)
	$hurtbox.position.x = 37 * sign(temp)
	$AnimationPlayer.play("attack")
#	move.x = (MAX_SPEED + DASH_VEL) * sign(temp)

func _apply_movement():
	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var offset : int
	
	if right:
		move.x += ACCEL
		offset = 1
	elif left:
		move.x -= ACCEL
		offset = -1
	else:
		move.x = move_toward(move.x,0,STOP_FORCE)
		currentState = states.IDLE
		return
	
	$hurtbox.position.x = 37 * offset
	move.x = clamp(move.x,-MAX_SPEED, MAX_SPEED)
	
	move_and_slide(move)

func _shoot():
	var shoot = Input.is_action_just_pressed("shoot")
	
	if shoot:
		if get_tree().get_current_scene().has_method("spawnBullet"):
			get_tree().get_current_scene().spawnBullet($bulletPos.global_position)

func getState() -> int:
	return currentState

func _on_hurtbox_body_entered(body):
	if body.has_method("punched"):
		if currentState == states.DASH:
			body.punched(sign($hurtbox.position.x))
		elif currentState == states.ATTACK:
			var vec_to_ball : Vector2 = $hurtbox.position - body.position
			print(rad2deg(vec_to_ball.angle()))

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "attack":
		currentState = states.IDLE
		attacking = false
