extends Area2D

func _on_tile_body_entered(body):
	if $ColorRect.color == ColorN("white"):
		$ColorRect.color = ColorN("green")
		get_tree().current_scene.tilesAmount -= 1
