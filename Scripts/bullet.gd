extends Area2D

const SPEED : float = 450.0

func _physics_process(delta):
	position.y -= SPEED * delta

func _on_bullet_body_entered(body):
	if body.has_method("shooted"):
		queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
