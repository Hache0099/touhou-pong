extends Node2D

const bullet = preload("res://Scenes/bullet.tscn")
const tile = preload("res://Scenes/tile.tscn")
const tile_size = Vector2(30,30)
const LEVEL_PATH : String = "res://Level_img/level_"

var tilesAmount : int = 0

var level_patron : int = 1

func _ready():
	getPatron()
	get_tree().set_pause(true)

func _on_VSlider_value_changed(value):
	Engine.set_time_scale(value)

func getPatron():
	var p = load(LEVEL_PATH + str(level_patron) + ".png")
	var img = p.get_data()
	img.lock()
	var size = img.get_size()
	
	for y in size.y:
		for x in size.x:
			if img.get_pixel(x,y) == ColorN("white"):
				createTile(Vector2(x,y))
				tilesAmount += 1
	print(tilesAmount)

func createTile(pos : Vector2):
	var t = tile.instance()
	t.position = pos * tile_size
	$tileHolder.add_child(t)

func spawnBullet(pos : Vector2):
	var b = bullet.instance()
	b.position = pos
	add_child(b)

func _on_Button_pressed():
	get_tree().set_pause(false)
	$Button.hide()
