extends RigidBody2D

const MIN_VEL : float = 300.0
const FIX_VEL_X : float = 295.0
const MIN_VEL_Y : float = 350.0
const FIX_VEL_Y : float = 310.0

func _ready():
	$Label.set_as_toplevel(true)

func _integrate_forces(state):
	var lin_vel = state.get_linear_velocity()
	
	if abs(lin_vel.x) < MIN_VEL and abs(lin_vel.x) > 1:
		set_axis_velocity(Vector2(FIX_VEL_X * sign(lin_vel.x),0))
	
	$Label.set_text(str(state.get_linear_velocity()))

func punched(hurtbox_dir : float):
	var angle = -90 + 35.0 * hurtbox_dir
	if hurtbox_dir != 0:
		set_linear_velocity(polar2cartesian(750.0,deg2rad(angle)))
	else:
		set_axis_velocity(Vector2(0,-700.0))

func punchIdle(hurtbox_pos : Vector2):
	var vec_to_ball = hurtbox_pos - position
	var dotted = Vector2.UP.dot(vec_to_ball.tangent().normalized())
	
	if abs(dotted) < (55.0/90.0):
		set_axis_velocity(Vector2(0,-700.0))
	else:
		var angle = -90 + 35 * sign(-dotted)
		set_linear_velocity(polar2cartesian(780.0,deg2rad(angle)))

func shooted(pos : Vector2):
	var vec = - pos + position
	set_linear_velocity(vec.normalized() * 600.0)

#func _on_Timer_timeout():
#	pass

#func _on_ball_body_exited(body):
#	if body.is_in_group("floor"):
#		if get_linear_velocity().y > -MIN_VEL_Y:
#			set_axis_velocity(Vector2(0,-FIX_VEL_Y))

func _on_hitbox_area_entered(area):
	if area.is_in_group("player_hurtbox"):
		var p = area.get_owner()
		
		if p.getState() == 2:
			punchIdle(area.get_global_position())
		else:
			punched(sign(area.get_position().x))
	elif area.is_in_group("bullet"):
		shooted(area.get_global_position())
